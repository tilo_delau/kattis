/**  
 * @author Tilo Delau
 * Aug 2018
 * A New Alphabet
 * Difficulty: 1.6
 * 
 */

import java.util.Scanner;
import java.util.HashMap;

public class ANewAlphabet {

	public static void main(String[] args) {
		HashMap<Character , String> map = new HashMap<>();
		final int totLetters = 26;
		char[] oldAlphabet = new char[totLetters];
		String[] newAlphabet = {"@","8","(","|)","3","#","6","[-]","|","_|","|<","1","[]\\/[]","[]\\[]","0","|D","(,)","|Z","$","']['","|_|","\\/","\\/\\/","}{","`/","2"};

		Scanner sc = new Scanner(System.in);

		for (int i = 0; i < totLetters; i++) oldAlphabet[i] = (char)(i + 97);

		for (int i = 0; i < totLetters; i++) map.put(oldAlphabet[i], newAlphabet[i]);

		String input = sc.nextLine().toLowerCase();
		String translation = "";

		for (int i = 0; i < input.length(); i++) {
			if (map.containsKey(input.charAt(i))) {
				translation += map.get(input.charAt(i));
			} else {
				translation += input.charAt(i);
			}
		}

		System.out.println(translation);
		sc.close();

	}

}