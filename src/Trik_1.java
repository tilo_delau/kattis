// Difficulty: 1.2
// https://open.kattis.com/problems/trik
// Tilo Delau 2018

import java.util.Arrays;
import java.util.Scanner;

public class Trik_1 {
	
	static int[] ball = {1,0,0};
	
	public static void main(String[] args) {
		String input;
		Scanner scan = new Scanner(System.in);
		input = scan.next();

		for (int i = 0; i < input.length(); i++) {
            moveBall(""+input.charAt(i));
        }
		
		System.out.println(""+(getBall()+1));
				
	}
	
	public static int getBall() {		
		int ballIndex = 0;
		
		for (int i = 0; i < ball.length; i++) {
			if (ball[i] == 1) {
				ballIndex = i;
			}
		}
						
		return ballIndex;
	}
	
	public static void moveBall(String cup) {
		
		switch(cup) {
		case "A": 
			if (getBall() == 0) {
				Arrays.fill(ball, 0);
				ball[1] = 1;
				break;
			}
			if (getBall() == 1) {
				Arrays.fill(ball, 0);
				ball[0] = 1;
				break;
			}
			if (getBall() == 2) {
				// Do nothing
				break;
			}
			
		case "B": 
			if (getBall() == 0) {
				// Ball is in left cup, this movement does not move the ball
				break;
			}
			if (getBall() == 1) {
				// Ball is in the middle, moves to right cup.
				Arrays.fill(ball, 0);
				ball[2] = 1;
				break;
			}
			if (getBall() == 2) {
				// Ball is in the right cup, move it to the middle
				Arrays.fill(ball, 0);
				ball[1] = 1;
				break;
			}
			
		case "C": 
			if (getBall() == 0) {
				Arrays.fill(ball, 0);
				ball[2] = 1;
				break;
			}
			if (getBall() == 1) {
				// Do nothing
				break;
			}
			if (getBall() == 2) {
				Arrays.fill(ball, 0);
				ball[0] = 1;
				break;
			}
		}
		
		
	}

}
