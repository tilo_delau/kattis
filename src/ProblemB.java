/**  
 * @author Tilo Delau
 * Aug 2018
 * Problem B
 * Kattis Cap Gemini Test Medium 
 *
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ProblemB {
	private static ArrayList<String> listPhoneNumbers;	// Store telephone numbers for each test case	
	private static int totTestCases;						// How many groups of tel numbers
	private static int totPhoneNumbers;					// tel numbers in each case.
	
	private static Scanner sc;
	
	public static void main(String[] args){		
		listPhoneNumbers	= new ArrayList<String>();
		sc = new Scanner(System.in);
			
		totTestCases = sc.nextInt();

		if(totTestCases > 0 && totTestCases < 41) {

			for(int i = 0; i < totTestCases; i++) {
				createListPhoneNumbers();
				if(checkConsistency()) {
					System.out.println("YES");
				}					
				else {
					System.out.println("NO");
				}
					
			}
		}

	}
	
	private static void createListPhoneNumbers() {
		totPhoneNumbers = sc.nextInt();
				
		if(totPhoneNumbers > 0 && totPhoneNumbers < 10001) {
			
			listPhoneNumbers.clear();
			
			for(int i = 0; i < totPhoneNumbers; i++) {
				String tmpNumber = sc.next();
				
				if(tmpNumber.length() < 11)
					listPhoneNumbers.add(tmpNumber);
			}
			
			Collections.sort(listPhoneNumbers);

		}
		
	}

	// In order to be inconsistent, a number in the list has to be at least a subset
	// of another whole number in the list. eg.:
	// 1234 and 12345 is inconsistent
	// while 1234 and 12304 is consistent. (123 is the same but the 4th digit is not)
	private static boolean checkConsistency() {
		
		// Go through and check the second number against the first number
		for(int i=0; i < (listPhoneNumbers.size()-1); i++) {
			String firstNumber = listPhoneNumbers.get(i);
			String secondNumber = listPhoneNumbers.get(i+1);
			
			// If the next number starts the same way as the previous number, 
			// it has the same prefix
			// So first, we check if it has as many or more digits than previous
			if(secondNumber.length() >= firstNumber.length()) {
				String tmpNumber;
				// We're only interested in the first part of the number
				tmpNumber = secondNumber.substring(0, firstNumber.length());
				
				if(tmpNumber.equals(firstNumber)) // We found an inconsistent number
					return false;				
			}
		}
		return true;
	}
	

	

}