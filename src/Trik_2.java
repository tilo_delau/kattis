// Difficulty: 1.2

import java.util.Scanner;

public class Trik_2 {

    public static void A(boolean[] a) {
        boolean temp = a[1];
        a[1] = a[0];
        a[0] = temp;
    }

    public static void B(boolean[] a) {
        boolean temp = a[2];
        a[2] = a[1];
        a[1] = temp;
    }

    public static void C(boolean[] a) {
        boolean temp = a[2];
        a[2] = a[0];
        a[0] = temp;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean[] arr = {true, false, false};
        String input = scan.nextLine();
        for (int i = 0; i < input.length(); i++) {
            switch (input.charAt(i)) {
                case 'A':
                    A(arr);
                    break;
                case 'B':
                    B(arr);
                    break;
                case 'C':
                    C(arr);
                    break;
            }
        }
        if(arr[0])
            System.out.println("1");
        else if(arr[1])
            System.out.println("2");
        else
              System.out.println("3");
            
    }
}