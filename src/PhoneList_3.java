import java.util.Arrays;
import java.util.Scanner;
public class PhoneList_3 {
public static void main(String[] args) {
	
	Scanner scan = new Scanner(System.in);

int cases = scan.nextInt();

for (int zax = 0; zax < cases; zax++)
	{
	int many = scan.nextInt();
	String[] nums = new String[many];
	
	for (int i = 0; i < many; i++)
		nums[i] = scan.next();
	
    Arrays.sort(nums);
    
    boolean consistent = true;
    
    for (int i = 1; i < nums.length; i++)
    	if (nums[i].startsWith(nums[i - 1]))
    		{
    		consistent = false;
    		break;
			}
    
    if (consistent)
    	System.out.println("YES");
    else
    	System.out.println("NO");
	}

scan.close();
	}
}