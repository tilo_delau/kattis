import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;


public class PhoneList {
	
	private ArrayList<String> numbers;	
	private Scanner sc;
	private int numbOfTestCase;
	private int numbOfPhoneNumbers;
	private String phoneNumb;
	
	public PhoneList(){
		this.numbers = new ArrayList<String>();
		this.sc = new Scanner(System.in);
		this.phoneNumb = new String();
	}
	
	private void init() throws InputMismatchException{
		numbOfTestCase = sc.nextInt();
		
		if(numbOfTestCase < 1 || numbOfTestCase > 40)
			throw new InputMismatchException("Testcase need to be in range 1 to 40.");
		
		for(int i = 0; i<numbOfTestCase;i++){
			addNumbers();
			if(checkIfConsistent())
				System.out.println("YES");
			else
				System.out.println("NO");
		}
	}

	private void addNumbers() throws InputMismatchException{
		numbOfPhoneNumbers = sc.nextInt();
		if(numbOfPhoneNumbers < 1 || numbOfPhoneNumbers > 10000)
			throw new InputMismatchException("Phonenumbers need to be in range 1 to 10000.");
		
		numbers.clear();
		
		for(int i = 0; i<numbOfPhoneNumbers; i++){
			phoneNumb = sc.next();
			if(phoneNumb.length() > 10)
				throw new InputMismatchException("Phonenumb is longer than 10 digits.");
			numbers.add(phoneNumb);
		}
		
		Collections.sort(numbers);
	}
	
	private boolean checkIfConsistent(){
		for(int i=0; i<numbOfPhoneNumbers-1; i++){
			String potentialPrefix = numbers.get(i);
			String couldHavePrefix = numbers.get(i+1);
			if(couldHavePrefix.length() >= potentialPrefix.length()){
				phoneNumb = couldHavePrefix.substring(0,potentialPrefix.length());
				if(phoneNumb.equals(potentialPrefix))
					return false;				
			}
		}
		return true;
	}
	
	public static void main(String[] args){
		try{
			new PhoneList().init();
		}catch(Exception E){
			System.out.println(E.getMessage());
		}
		
	}
	

}